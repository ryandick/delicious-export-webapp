import DS from 'ember-data';
export default DS.Model.extend( {
	token: DS.attr( 'string' ),
	distance: DS.attr( 'number' ),
	idf: DS.attr( 'number' ),
	count: DS.attr( 'number' )
} );
