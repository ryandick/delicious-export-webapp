import DS from 'ember-data';
export default DS.Model.extend( {
	query: DS.attr( 'string' ),
	url: DS.attr( 'string' ),
	title: DS.attr( 'string' ),
	summary: DS.attr( 'string' ),
	statusCode: DS.attr( 'number' ),
	added: DS.attr( 'date' ),
	updated: DS.attr( 'date' ),
	score: DS.attr( 'number' )
} );
