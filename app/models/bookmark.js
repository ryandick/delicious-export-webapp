import DS from 'ember-data';
export default DS.Model.extend( {
	url: DS.attr( 'string' ),
	title: DS.attr( 'string' ),
	summary: DS.attr( 'string' ),
	text: DS.attr( 'string' ),
	statusCode: DS.attr( 'number' ),
	added: DS.attr( 'date' ),
	updated: DS.attr( 'date' )
} );
