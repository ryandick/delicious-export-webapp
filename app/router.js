import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
 this.route( 'bookmarks', {
    path: '/bookmarks/:page',
  } );
  this.route( 'bookmark', {
    path: '/bookmark/:id'
  } );
  this.route('search');
});

export default Router;
