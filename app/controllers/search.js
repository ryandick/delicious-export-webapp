import Ember from 'ember';
import debounce from '../utils/debounce';
/*global $*/
let __keyPress = function ( query, e ) {
	if ( query.length > 3 ) {
		let selectedSuggestionIndex = this.get( 'selectedSuggestionIndex' ) || null;
		if ( e.keyCode === 38 || e.keyCode === 40 ) { //up or down arrow
			e.preventDefault();
			if ( e.keyCode === 38 ) { //up
				selectedSuggestionIndex--;
				selectedSuggestionIndex = ( selectedSuggestionIndex >= -1 ) ? selectedSuggestionIndex : -1; // 0 or more
			} else { //down
				selectedSuggestionIndex++;
				selectedSuggestionIndex = ( selectedSuggestionIndex < 15 ) ? selectedSuggestionIndex : 14; // less than 15
			}
			this.set( 'selectedSuggestionIndex', selectedSuggestionIndex );
			$( `.suggestion` ).each( ( index, elm ) => {
				if ( index === selectedSuggestionIndex ) {
					$( elm ).addClass( 'selected' );
				} else {
					$( elm ).removeClass( 'selected' );
				}
			} );
		} else {
			if ( e.keyCode === 13 ) { //enter
				if ( selectedSuggestionIndex >= 0 ) {
					let suggestionText = $( '.selected' ).text();
					query = suggestionText.trim();
					this.set( 'query', query );
					this.set( 'queryField', query );
					this.set( 'action', 'search' );
					this.set( 'isSuggest', false );
				} else {
					if ( this.get( 'action' ) !== 'search' ) {
						this.set( 'action', 'search' );
						this.set( 'isSuggest', false );
					}
					this.set( 'query', query );
					this.set( 'isLoading', true );
				}
			} else {
				if ( this.get( 'action' ) !== 'suggest' ) {
					this.set( 'action', 'suggest' );
					this.set( 'isSuggest', true );
				}
				this.set( 'query', query );
				this.set( 'isLoading', false );
			}
		}
	}
};
export default Ember.Controller.extend( {
	queryParams: [ 'query', 'action' ],
	query: null,
	action: 'search',
	isLoading: true,
	isSuggest: true,
	selectedSuggestionIndex: -1,
	queryField: Ember.computed.oneWay( 'query' ),
	actions: {
		keyPress: debounce( __keyPress, 200 )
	}
} );
