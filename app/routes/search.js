import Ember from 'ember';
export default Ember.Route.extend( {
	controllerName: 'search',
	model( params ) {
		let page = this.get( 'page' );
		let action = this.get( 'action' ) || params.action || 'search';
		let limit = this.get( 'limit' );
		params.limit = limit;
		params.offset = ( page * params.limit );
		return this.store.query( action, params );
	},
	queryParams: {
		query: {
			refreshModel: true,
			replace: true
		},
		action: {
			refreshModel: true
		}
	},
	page: 0,
	offset: 0,
	limit: 25,
	setupController: ( controller, model ) => {
		let action = controller.get( 'action' );
		controller.set( `${action}Model`, model );
		controller.set( 'isLoading', false );
		controller.set( 'selectedSuggestionIndex', -1 );
		controller.set( 'heatmap', model.meta.heatmap );
		controller.set( 'setTotal', model.meta.resultCount );
		controller.set( 'total', model.meta.total );
		controller.set( 'took', model.meta.took );
		controller.set( 'page', model.meta.page );
		controller.set( 'nextPage', model.meta.nextPage );
		controller.set( 'prevPage', model.meta.prevPage );
	}
} );
