import Ember from 'ember';
export default Ember.Route.extend( {
	model: function ( params ) {
		let pageNumber = this.page = Number( params.page || 0 );
		return this.store.query( 'bookmark', {
			limit: 20,
			offset: pageNumber * 20
		} );
	},
	page: 0
} );
