import Ember from 'ember';
/* global calendarHeatmap */
/* global d3 */
/* global moment */
export default Ember.Component.extend( {
	didReceiveAttrs: function () {
		let self = this;
		let chartData = this.get( 'data' );
		if ( chartData && chartData.length > 0 ) {
			d3.select( '.calendar-heatmap' ).remove();
			setTimeout( function () {
				calendarHeatmap().selector( '.chm' ).tooltipEnabled( true ).legendEnabled( true ).data( chartData )();
			}, 0 );
		}
	},
	didInsertElement: function () {
		console.log( 'didInsertElement', arguments );
	}
} );
