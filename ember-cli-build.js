/*jshint node:true*/
/* global require, module */
var EmberApp = require( 'ember-cli/lib/broccoli/ember-app' );
module.exports = function ( defaults ) {
	var app = new EmberApp( defaults, {
		// Add options here
	} );
	// Use `app.import` to add additional libraries to the generated
	// output files.
	//
	// If you need to use different assets in different
	// environments, specify an object as the first parameter. That
	// object's keys should be the environment name and the values
	// should be the asset to use in that environment.
	//
	// If the library that you are including contains AMD or ES6
	// modules that you would like to import into your application
	// please specify an object with the list of modules as keys
	// along with the exports of each module as its value.
	//----- Styles ----//
	app.import( 'bower_components/Materialize/dist/css/materialize.css' );
	app.import( 'bower_components/calendar-heatmap/src/calendar-heatmap.css' );
	//------ Libs --- //
	app.import( 'bower_components/Materialize/dist/js/materialize.js' );
	app.import( 'bower_components/moment/moment.js' );
	app.import( 'bower_components/d3/d3.js' );
	app.import( 'bower_components/calendar-heatmap/src/calendar-heatmap.js' );
	//------ Fonts --//
	app.import( 'bower_components/Materialize/dist/font/roboto/Roboto-Light.woff', {
		destDir: 'font/roboto'
	} );
	app.import( 'bower_components/Materialize/dist/font/roboto/Roboto-Light.woff2', {
		destDir: 'font/roboto'
	} );
	app.import( 'bower_components/Materialize/dist/font/roboto/Roboto-Light.ttf', {
		destDir: 'font/roboto'
	} );
	app.import( 'bower_components/Materialize/dist/font/roboto/Roboto-Regular.woff', {
		destDir: 'font/roboto'
	} );
	app.import( 'bower_components/Materialize/dist/font/roboto/Roboto-Regular.woff2', {
		destDir: 'font/roboto'
	} );
	app.import( 'bower_components/Materialize/dist/font/roboto/Roboto-Regular.tff', {
		destDir: 'font/roboto'
	} );
	return app.toTree();
};
